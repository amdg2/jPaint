package beanartist.view;

import beanartist.model.Shape;

import java.awt.*;

/**
 * The view class manage the display of shape on screen
 */
public class ShapeView implements IView {
    private Shape modelObject;

    public ShapeView(Shape modelObject) {
        this.modelObject = modelObject;
    }

    public Shape getModelObject() {
        return modelObject;
    }

    public void setModelObject(Shape modelObject) {
        this.modelObject = modelObject;
    }

    public void display(ViewPanel panel, Graphics2D g2D) {
        Color oldColor = g2D.getColor();
        g2D.setColor(modelObject.getLineColor());
        g2D.setStroke(modelObject.getStroke());

        g2D.draw(modelObject);
        if (modelObject.isFillShape()) {
            g2D.fill(modelObject);
        }
        g2D.setColor(oldColor);
    }
}
