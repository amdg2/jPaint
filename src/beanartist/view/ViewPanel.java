package beanartist.view;

import beanartist.controller.HistoryController;
import beanartist.controller.strings.ITool;
import beanartist.controller.strings.S;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 * ViewPanel class
 */
public class ViewPanel extends JPanel {

    protected final static int DEFAULT_WIDTH = 800;
    protected final static int DEFAULT_HEIGHT = 600;
    protected final static Color DEFAULT_BACKGROUND_COLOR = Color.white;
    protected final static Color DEFAULT_DRAW_COLOR = Color.black;
    protected final static Stroke DEFAULT_DRAW_STROKE = new BasicStroke();

    private BasicFrame mainFrame;
    private ArrayList<IView> shapeViews;
    private Color currentDrawColor;
    private Stroke currentStroke;
    private ITool currentTool;
    private HistoryController history;

    private BufferedImage image;

    public ViewPanel(int width, int height, Color backgroundColor) {
        this.setBackground(backgroundColor);
        this.setPreferredSize(new Dimension(width, height));
        shapeViews = new ArrayList<>();
        history = new HistoryController(this);
        currentDrawColor = DEFAULT_DRAW_COLOR;
        currentStroke = DEFAULT_DRAW_STROKE;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);

        Graphics2D g2D = (Graphics2D)g;

        try {
            if(image != null)
                g2D.drawImage(image, 0, 0, null);

            g.setColor(currentDrawColor);
            for(IView aShapeView : shapeViews) {
                aShapeView.display(this, g2D);
            }
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this.getBasicFrame(),
                    S.get("Error while painting."),
                    String.format(S.get("Error while painting.\n%s"), e.toString()),
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    public void unselectTool() {
        // Free current tool if needed
        if(currentTool != null)
            currentTool.free();

        // Set current tool to null
        currentTool = null;
    }

    public BufferedImage getBufferedImage() {
        BufferedImage bi = new BufferedImage(this.getWidth(), this.getHeight(), BufferedImage.TYPE_INT_RGB);
        Graphics2D g = bi.createGraphics();
        this.paint(g);
        return bi;
    }

    public void clear() {
        this.shapeViews.clear();
        this.history.clear();
        this.image = null;
        this.repaint();
    }

    public void setBufferedImage(BufferedImage bi) {
        this.shapeViews.clear();
        this.history.clear();
        this.image = bi;
        this.setPreferredSize(new Dimension(bi.getWidth(), bi.getHeight()));
        this.repaint();
        mainFrame.pack();
    }

    public HistoryController getHistory() { return this.history; }

    public void addView(IView aShapeView) {
        shapeViews.add(aShapeView);
    }

    public void removeLastView() {
        if(!history.isEmpty()) {
            IView aShapeView;
            aShapeView = history.popView();
            shapeViews.remove(aShapeView);
            repaint();
        }
    }

    public void removeView(IView aShapeView) {
        shapeViews.remove(aShapeView);
        history.removeView(aShapeView);
    }

    public void setBasicFrame(BasicFrame mainFrame) {
        this.mainFrame = mainFrame;
        this.mainFrame.setViewPanel(this);
    }

    public Color getCurrentDrawColor() {
        return currentDrawColor;
    }

    public void setCurrentDrawColor(Color currentDrawColor) {
        this.currentDrawColor = currentDrawColor;

        if(currentTool != null)
            this.currentTool.setColor(currentDrawColor);
    }

    public Stroke getCurrentStroke(){return currentStroke;}

    public void setCurrentStroke(Stroke currentStroke){
        this.currentStroke = currentStroke;

        if(currentStroke != null)
            this.currentTool.setStroke(currentStroke);
    }

    public ITool getCurrentTool() {
        return currentTool;
    }

    public void setCurrentTool(ITool currentTool) {
        this.currentTool = currentTool;
    }

    public BasicFrame getBasicFrame() {
        return mainFrame;
    }
}
