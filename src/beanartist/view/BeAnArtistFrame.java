package beanartist.view;

import beanartist.controller.KeyboardController;

import java.awt.*;

/**
 * BeAnArtistFrame class
 */
public class BeAnArtistFrame extends BasicFrame {

    private MenuPanel menuPanel;
    private KeyboardController keyboardController;

    public BeAnArtistFrame() {
        this(BasicFrame.DEFAULT_WIDTH,
             BasicFrame.DEFAULT_HEIGHT,
             BasicFrame.DEFAULT_TITLE,
             BasicFrame.DEFAULT_BK_COLOR);
    }

    public BeAnArtistFrame(String title) {
        this(BasicFrame.DEFAULT_WIDTH,
                BasicFrame.DEFAULT_HEIGHT,
                title,
                BasicFrame.DEFAULT_BK_COLOR);
    }

    public BeAnArtistFrame(int width, int height) {
        this(width,
             height,
             BasicFrame.DEFAULT_TITLE,
             BasicFrame.DEFAULT_BK_COLOR);
    }

    public BeAnArtistFrame(int width, int height, String title, Color backgroundColor) {
        super(width, height, title, backgroundColor);
        associateMenuPanel();

        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(new KeyboardController(this));
    }

    private void associateMenuPanel() {
        System.out.println("Create BeAnArtistFrame.menuPanel");
        menuPanel = new MenuPanel(this);
        this.getContentPane().add(menuPanel, BorderLayout.NORTH);
        pack();
    }

    public MenuPanel getMenuPanel() {
        return menuPanel;
    }

    public void setMenuPanel(MenuPanel menuPanel) {
        this.menuPanel = menuPanel;
    }

}
