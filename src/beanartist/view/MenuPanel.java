package beanartist.view;

import beanartist.controller.*;
import beanartist.controller.strings.ITool;
import beanartist.controller.strings.S;

import javax.swing.*;
import java.awt.*;

/**
 * MenuPanel class
 */
public class MenuPanel extends JPanel {

    /**
     * Constant
     */
    private static final int DEFAULT_SLIDER_MIN = 0;
    private static final int DEFAULT_SLIDER_MAX = 100;
    private static final int DEFAULT_SLIDER_INIT = 0;

    private JToggleButton lineButton = new JToggleButton();
    private JToggleButton rectangleButton = new JToggleButton();
    private JToggleButton ellipseButton = new JToggleButton();
    private JToggleButton squareButton = new JToggleButton();
    private JToggleButton circleButton = new JToggleButton();
    private JToggleButton freeHandLineButton = new JToggleButton();
    private JToggleButton bucketFillButton = new JToggleButton();
    private JToggleButton noTool = new JToggleButton();
    private JToggleButton eraseButton = new JToggleButton();
    private ButtonGroup shapeGroup = new ButtonGroup();
    private ButtonGroup colorGroup = new ButtonGroup();
    private JButton colorChooser = new JButton();
    private JPanel colorChoosed = new JPanel();
    private JToggleButton fillButton = new JToggleButton();
    private JSlider slider = new JSlider(DEFAULT_SLIDER_MIN,DEFAULT_SLIDER_MAX,DEFAULT_SLIDER_INIT);

    ColorChooserFrame ccf = new ColorChooserFrame();

    BeAnArtistFrame beAnArtistFrame;

    public MenuPanel(BeAnArtistFrame myFrame){
        beAnArtistFrame = myFrame;
        initComponents();
    }

    public BeAnArtistFrame getFrame(){
        return beAnArtistFrame;
    }

    public void setFrame(BeAnArtistFrame beAnArtistFrame){
        this.beAnArtistFrame = beAnArtistFrame;
    }

    public void initComponents(){
        /**
         * No tool
         */
        instantiateShapeButton(noTool, shapeGroup, S.get("No tool"));
        noTool.setSelected(true);
        noTool.addActionListener(e1 -> {
            beAnArtistFrame.getViewPanel().unselectTool();
            unselectTool();
        });

        /**
         * Slider
         */
        slider.addChangeListener(e -> {
            ITool currentTool = beAnArtistFrame.getViewPanel().getCurrentTool();
            currentTool.setStroke(new BasicStroke(slider.getValue()));
            beAnArtistFrame.getViewPanel().setCurrentStroke(currentTool.getStroke());

        });
        this.add(slider);
        /**
         * Fill Button
         */
        instantiateShapeButton(fillButton, null, S.get("Fill"));
        fillButton.addActionListener(e -> {
            ITool currentTool = beAnArtistFrame.getViewPanel().getCurrentTool();

            if (fillButton.isSelected()) {
                colorGroup.setSelected(fillButton.getModel(), false);
            }

            currentTool.setFillShape(fillButton.isSelected());
        });

        /**
         * Line
         */
        instantiateShapeButton(lineButton,shapeGroup,S.get("Line"), LineTool.class);

        /**
         * Rectangle
         */
        instantiateShapeButton(rectangleButton,shapeGroup,S.get("Rectangle"), RectangleTool.class);

        /**
         * Square
         */
        instantiateShapeButton(squareButton,shapeGroup,S.get("Square"), SquareTool.class);

        /**
         * Ellipse
         */
        instantiateShapeButton(ellipseButton,shapeGroup,S.get("Ellipse"), EllipseTool.class);

        /**
         * Circle
         */
        instantiateShapeButton(circleButton,shapeGroup,S.get("Circle"), CircleTool.class);

        /**
         * Free Hand Line
         */
        instantiateShapeButton(freeHandLineButton, shapeGroup, S.get("Free Hand Line"), FreeHandLineTool.class);

        /**
         * Bucket fill
         */
        instantiateShapeButton(bucketFillButton, shapeGroup, S.get("Bucket"), BucketFillTool.class);


        /**
         * Erase Button
         */
        instantiateEraseButton(eraseButton,shapeGroup,S.get("Erase"));


        /**
         * Color
         */
        colorChoosed = new JPanel();
        colorChoosed.setPreferredSize(new Dimension(50,25));
        colorChoosed.setBackground(beAnArtistFrame.getViewPanel().getCurrentDrawColor());
        this.add(colorChoosed);

        /**
         * Color chooser
         */
        colorChooser.setText("Color Chooser");
        colorChooser.addActionListener(e -> {
            ccf.setColor(beAnArtistFrame.getViewPanel().getCurrentDrawColor());
            ccf.setVisible(true);
        });
        this.add(colorChooser);

        /**
         * Color chooser dialog
         */
        ccf.addChangeListener(e -> {
            beAnArtistFrame.getViewPanel().setCurrentDrawColor(ccf.getColor());
            colorChoosed.setBackground(ccf.getColor());
        });

    }


    private void instantiateShapeButton(JToggleButton button, ButtonGroup group, String text) {
        if(group != null)
            group.add(button);
        button.setText(text);
        this.add(button);
    }

    /**
     *
     * @param button Button to instantiate
     * @param group Button's group
     * @param textButton Button's text
     * @param tClass Button's tool class
     * @param <T> Button's tool class
     */
    private <T extends ITool> void instantiateShapeButton(JToggleButton button,ButtonGroup group, String textButton, Class<T> tClass ){
        group.add(button);
        button.setText(textButton);
        button.addActionListener(e -> _onClick(tClass));
        this.add(button);
    }

    private void instantiateEraseButton(JToggleButton button, ButtonGroup group, String textButton){
        group.add(button);
        button.setText(textButton);
        button.addActionListener(e -> {
            ITool oldTool = beAnArtistFrame.getViewPanel().getCurrentTool();
            if (oldTool != null)
                oldTool.free();
            FreeHandLineTool freeHandLineTool = new FreeHandLineTool();
            freeHandLineTool.setColor(Color.white);
            freeHandLineTool.setFillShape(false);
            freeHandLineTool.setStroke((beAnArtistFrame.getViewPanel().getCurrentStroke()));
            freeHandLineTool.associate(beAnArtistFrame.getViewPanel());
        });
        this.add(button);
    }
    /**
     *
     * @param temp Tool class
     * @param <T> Tool class
     */
    private <T extends ITool> void _onClick(Class<T> temp)  {
        ITool oldTool = beAnArtistFrame.getViewPanel().getCurrentTool();
        if (oldTool != null)
            oldTool.free();

        ITool tool;
        try {
            tool = temp.newInstance();
            tool.setColor(beAnArtistFrame.getViewPanel().getCurrentDrawColor());
            tool.setFillShape(fillButton.isSelected());
            tool.setStroke((beAnArtistFrame.getViewPanel().getCurrentStroke()));
            tool.associate(beAnArtistFrame.getViewPanel());
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(beAnArtistFrame,
                    String.format(S.get("Unable to create tool %s."), temp.getName()),
                    S.get("Error while creating tool."),
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    public void unselectTool() {
        this.noTool.setSelected(true);
    }
}
