package beanartist.view;

import beanartist.view.menu.Edit;
import beanartist.view.menu.File;

import javax.swing.*;

/**
 * Main menu bar
 */
public class Menu extends JMenuBar {

    File menuFile;
    Edit menuEdit;

    public Menu(BasicFrame frame) {
        init(frame);
    }

    private void init(BasicFrame frame) {
        initializeMenuFile(frame);
        initializeMenuEdit(frame);

        add(menuFile);
        add(menuEdit);
    }

    private void initializeMenuFile(BasicFrame frame) {
        menuFile = new File(frame);
    }

    private void initializeMenuEdit(BasicFrame frame) {
        menuEdit = new Edit(frame);
    }

    public void updateMenuStatus() {
        menuEdit.updateMenuStatus();
    }
}
