package beanartist.view.menu;

import beanartist.controller.MenuController;
import beanartist.controller.strings.S;
import beanartist.view.BasicFrame;

import javax.swing.*;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

/**
 * Menu "Edit"
 */
public class Edit extends JMenu {

    private MenuController controller;
    private BasicFrame frame;

    public Edit(BasicFrame frame) {
        super(S.get("Edit"));
        controller = new MenuController(frame);
        this.frame = frame;
        init();
        updateMenuStatus();
    }

    private void init() {
        cancel = new JMenuItem(S.get("Cancel"));
        clear = new JMenuItem(S.get("Clear all"));

        cancel.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z, InputEvent.CTRL_DOWN_MASK));

        cancel.addActionListener(e -> controller.cancelAction());
        clear.addActionListener(e -> controller.clearView());

        add(cancel);
        add(clear);
    }

    public void updateMenuStatus() {
        cancel.setEnabled(!frame.getViewPanel().getHistory().isEmpty());
    }

    JMenuItem clear;
    JMenuItem cancel;
}
