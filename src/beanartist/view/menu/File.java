package beanartist.view.menu;

import beanartist.controller.MenuController;
import beanartist.controller.strings.S;
import beanartist.view.BasicFrame;

import javax.swing.*;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

/**
 * Menu "File"
 */
public class File extends JMenu {

    MenuController controller;

    public File(BasicFrame frame) {
        super(S.get("File"));
        controller = new MenuController(frame);
        init();
    }

    private void init() {
        open = new JMenuItem(S.get("Open"));
        save = new JMenuItem(S.get("Save"));
        exit = new JMenuItem(S.get("Exit"));

        open.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_DOWN_MASK));
        save.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK));
        exit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, InputEvent.CTRL_DOWN_MASK));

        open.addActionListener(e -> controller.openFile());
        save.addActionListener(e -> controller.saveFile());
        exit.addActionListener(e -> controller.exitApplication());

        add(open);
        add(save);
        add(exit);
    }

    JMenuItem open;
    JMenuItem save;
    JMenuItem exit;
}
