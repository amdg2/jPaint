package beanartist.view;

import javax.swing.*;
import java.awt.*;

/**
 * BasicFrame class
 * Used to manage the main window of the application
 */
public class BasicFrame extends JFrame {

    public static final String DEFAULT_TITLE = "Be An Artist";
    public static final int DEFAULT_WIDTH = 800;
    public static final int DEFAULT_HEIGHT = 600;
    public static final Color DEFAULT_BK_COLOR = Color.white;

    private Menu menu;

    private ViewPanel viewPanel;

    private JScrollPane scrollPane;

    public BasicFrame() {
        this(DEFAULT_WIDTH, DEFAULT_HEIGHT, DEFAULT_TITLE, DEFAULT_BK_COLOR);
    }

    public BasicFrame(String title) {
        this(DEFAULT_WIDTH, DEFAULT_HEIGHT, title, DEFAULT_BK_COLOR);
    }

    public BasicFrame(int width, int height) {
        this(width, height, DEFAULT_TITLE, DEFAULT_BK_COLOR);
    }

    public BasicFrame(int width, int height, String title, Color backgroundColor) {
        super(title);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        associateViewPanel(width, height, backgroundColor);
        attachMenu();
        pack();
        this.setVisible(true);
    }

    private void associateViewPanel(int width, int height, Color backgroundColor) {
        viewPanel = new ViewPanel(width, height, backgroundColor);
        viewPanel.setBasicFrame(this);

        scrollPane = new JScrollPane(viewPanel);
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        scrollPane.setBounds(0, 0, width, height);
        this.add(scrollPane);
    }

    private void attachMenu() {
        menu = new Menu(this);
        setJMenuBar(menu);
    }

    public ViewPanel getViewPanel() {
        return viewPanel;
    }

    public void setViewPanel(ViewPanel viewPanel) {
        this.viewPanel = viewPanel;
    }

    public Menu getMenu() {
        return this.menu;
    }
}
