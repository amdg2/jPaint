package beanartist.view;

import beanartist.controller.strings.S;

import javax.swing.*;
import javax.swing.event.ChangeListener;
import java.awt.*;

/**
 * Color Chooser window
 */
public class ColorChooserFrame extends JFrame {

    protected static final String DEFAULT_PANEL_TITLE = "Color Panel";


    private JPanel preview;
    private JColorChooser colorChooser;
    private JButton okButton;
    private JPanel menuBar;
    private JLabel previewLabel;

    public ColorChooserFrame() {
        this(Color.black);
    }

    public ColorChooserFrame(Color color){
        super(DEFAULT_PANEL_TITLE);
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        this.setLayout(new BorderLayout());

        // Init color chooser
        colorChooser = new JColorChooser();
        colorChooser.setColor(color);
        colorChooser.setPreviewPanel(new JPanel());
        this.add(colorChooser, BorderLayout.CENTER);

        // Menu bar
        menuBar = new JPanel();
        add(menuBar, BorderLayout.SOUTH);

        // Preview Label
        previewLabel = new JLabel(S.get("Selected color: "));
        menuBar.add(previewLabel);

        // Preview button
        preview = new JPanel();
        preview.setBackground(color);
        preview.setPreferredSize(new Dimension(200, 50));
        menuBar.add(preview);

        // Update preview button color
        addChangeListener(e -> preview.setBackground(getColor()));

        // Init ok button
        okButton = new JButton(S.get("Ok"));
        okButton.addActionListener(e -> this.setVisible(false));
        menuBar.add(okButton);

        pack();
        this.setVisible(false);
    }

    public void setColor(Color color) {
        colorChooser.setColor(color);
        preview.setBackground(color);
    }

    public Color getColor() {
        return colorChooser.getColor();
    }

    public void addChangeListener(ChangeListener listener) {
        colorChooser.getSelectionModel().addChangeListener(listener);
    }
}
