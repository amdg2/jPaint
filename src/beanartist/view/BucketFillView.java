package beanartist.view;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Bucket Fill view
 */
public class BucketFillView implements IView {
    private Point start;
    private Color targetColor;
    private Color replaceColor;
    private boolean disableBucket = false;
    private BufferedImage filledImage = null;

    public BucketFillView(Point start, Color replaceColor) {
        this.start = start;
        this.replaceColor = replaceColor;
    }

    private static void FloodFill(BufferedImage image, Point startPixel, Color targetColor, Color replaceColor) {
        if(targetColor.equals(replaceColor))
            return;

        Stack<Point> points = new Stack<>();
        List<Point> processed = new ArrayList<>();

        points.push(startPixel);

        int targetColorRGB = targetColor.getRGB();
        int replaceColorRGB = replaceColor.getRGB();
        int pixelColorRGB;

        while(!points.isEmpty()) {
            Point pixel = points.pop();

            try {
                if (pixel.x >= 0 && pixel.y >= 0) {
                    pixelColorRGB = image.getRGB(pixel.x, pixel.y);
                    if (pixelColorRGB == targetColorRGB) {
                        image.setRGB(pixel.x, pixel.y, replaceColorRGB);
                        points.push(new Point(pixel.x - 1, pixel.y));
                        points.push(new Point(pixel.x + 1, pixel.y));
                        points.push(new Point(pixel.x, pixel.y - 1));
                        points.push(new Point(pixel.x, pixel.y + 1));
                    }
                }
            } catch(ArrayIndexOutOfBoundsException ex) {
                pixel = null;
            }
        }
    }

    public void display(ViewPanel panel, Graphics2D g2D) {
        if(!disableBucket) {
            if(filledImage == null) {
                System.out.println("Create buffered image");
                filledImage = new BufferedImage(panel.getWidth(), panel.getHeight(), BufferedImage.TYPE_INT_RGB);
                Graphics2D g = filledImage.createGraphics();

                disableBucket = true;
                panel.paint(g);
                disableBucket = false;

                int[] targetColors = new int[3];
                filledImage.getRaster().getPixel(start.x, start.y, targetColors);
                targetColor = new Color(targetColors[0], targetColors[1], targetColors[2]);

                FloodFill(filledImage, start, targetColor, replaceColor);
            }

            g2D.drawImage(filledImage, 0, 0, null);
        }
    }
}
