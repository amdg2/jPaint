package beanartist.view;

import java.awt.*;

/**
 * View interface
 */
public interface IView {
    void display(ViewPanel panel, Graphics2D g2D);
}
