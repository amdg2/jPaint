package beanartist.test.model;

import beanartist.model.Square;

import java.awt.geom.Point2D;

/**
 * Created by Kevin on 22/10/2015.
 */
public class SquareTest {
    private static void testConstructorNoParam(){
        System.out.println("Constructor with no parameter");
        Square testSquare = new Square();
        System.out.println(testSquare);

    }

    private static void testConstructorWithParam(){
        System.out.println("Constructor with parameters");
        Square testSquare = new Square(200,180,37,2);
        System.out.println(testSquare);

    }

    private static void testConstructorWithPos(){
        System.out.println("Constructor with pos parameter");
        Point2D.Double testPos = new Point2D.Double(33,85);
        Square testSquare = new Square(testPos,100,220);
        System.out.println(testSquare);

    }

    public static void main(String[] args) {
        testConstructorNoParam();
        testConstructorWithParam();
        testConstructorWithPos();

    }
}
