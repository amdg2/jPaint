package beanartist.test.model;

import beanartist.model.Circle;

import java.awt.geom.Point2D;

/**
 * Created by Kevin on 22/10/2015.
 */
public class CircleTest {

    private static void testConstructorNoParam(){
        System.out.println("Constructor with no parameter");
        Circle testCircle = new Circle();
        System.out.println(testCircle);

    }

    private static void testConstructorWithParam(){
        System.out.println("Constructor with parameters");
        Circle testCircle = new Circle(150,200,85,20);
        System.out.println(testCircle);

    }

    private static void testConstructorWithPos(){
        System.out.println("Constructor with pos parameter");
        Point2D.Double testPos = new Point2D.Double(350,20);
        Circle testCircle = new Circle(testPos,500,20);
        System.out.println(testCircle);

    }

    public static void main(String[] args) {
        testConstructorNoParam();
        testConstructorWithParam();
        testConstructorWithPos();

    }

}
