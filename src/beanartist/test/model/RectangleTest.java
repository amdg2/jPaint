package beanartist.test.model;

import beanartist.model.Rectangle;

import java.awt.geom.Point2D;

/**
 * Created by Kevin on 22/10/2015.
 */
public class RectangleTest {


    private static void testConstructornoParam(){
        System.out.println("Constructor with no parameter");
        Rectangle testRectangle = new Rectangle();
        System.out.println(testRectangle);

    }

    private static void testConstructorWithParam(){
        System.out.println("Constructor with parameters");
        Rectangle testRectangle = new Rectangle(150,200,85,20);
        System.out.println(testRectangle);

    }

    private static void testConstructorWithPos(){
        System.out.println("Constructor with pos parameter");
        Point2D.Double testPos = new Point2D.Double(350,20);
        Rectangle testRectangle = new Rectangle(testPos,500,20);
        System.out.println(testRectangle);

    }

    public static void main(String[] args) {
        testConstructornoParam();
        testConstructorWithParam();
        testConstructorWithPos();

    }

}
