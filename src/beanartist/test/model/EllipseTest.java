package beanartist.test.model;

import beanartist.model.Ellipse;

import java.awt.geom.Point2D;

/**
 * Created by Kevin on 22/10/2015.
 */
public class EllipseTest {

    private static void testConstructorNoParam(){
        System.out.println("Constructor with no parameter");
        Ellipse testEllipse = new Ellipse();
        System.out.println(testEllipse);

    }

    private static void testConstructorWithParam(){
        System.out.println("Constructor with parameters");
        Ellipse testEllipse = new Ellipse(150,200,85,20);
        System.out.println(testEllipse);

    }

    private static void testConstructorWithPos(){
        System.out.println("Constructor with pos parameter");
        Point2D.Double testPos = new Point2D.Double(350,20);
        Ellipse testEllipse = new Ellipse(testPos,500,20);
        System.out.println(testEllipse);

    }

    public static void main(String[] args) {
        testConstructorNoParam();
        testConstructorWithParam();
        testConstructorWithPos();

    }
}
