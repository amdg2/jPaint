package beanartist.test.model;

import beanartist.model.Line;

/**
 * Created by Kevin on 22/10/2015.
 */
public class LineTest {

    private static void testConstructorWithParam(){
        System.out.println("Constructor with parameters");
        Line testLine = new Line(150,200,78,49);
        System.out.println(testLine);
    }

    private static void testmoveTo(){
        System.out.println("Test functionnality moveP1To, moveP2To and moveShapeTo");
        Line testLine = new Line(100,200,300,400);
        System.out.println(testLine);
        testLine.moveShapeTo(10,10);
        System.out.println(testLine);
        testLine.moveP1To(50, 658);
        System.out.println(testLine);
        testLine.moveP2To(800,800);
        System.out.println(testLine);



    }

    public static void main(String[] args) {
        testConstructorWithParam();
        testmoveTo();
    }
}
