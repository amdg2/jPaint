package beanartist.test.view;

import beanartist.model.Circle;
import beanartist.model.Rectangle;
import beanartist.model.Shape;
import beanartist.model.Square;
import beanartist.view.BasicFrame;
import beanartist.view.ShapeView;

import java.awt.*;

/**
 * Test class for view.ViewPanel
 */
public class ViewPanelTest {

    private static void test() {
        BasicFrame frame = new BasicFrame("ViewPanelTest");

        Shape rectangle = new Rectangle(10, 10, 70, 50);
        Shape square = new Square(30, 30, 25, 25);
        Shape circle = new Circle(20, 55, 100, 100);

        rectangle.setLineColor(Color.red);
        square.setLineColor(Color.blue);

        frame.getViewPanel().addView(new ShapeView(rectangle));
        frame.getViewPanel().addView(new ShapeView(square));
        frame.getViewPanel().addView(new ShapeView(circle));

        frame.repaint();
    }

    public static void main(String args[]) {
        test();
    }
}
