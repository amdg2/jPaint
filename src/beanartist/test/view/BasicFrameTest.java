package beanartist.test.view;

import beanartist.view.BasicFrame;

import java.awt.*;

/**
 * Test class for BasicFrame
 */
public class BasicFrameTest {

    private static void testConstructorNoParam() {
        BasicFrame frame = new BasicFrame();
    }

    private static void testConstructorName() {
        BasicFrame frame = new BasicFrame("Constructor test");
    }

    private static void testConstructorWidthHeight() {
        BasicFrame frame = new BasicFrame(640, 480);
    }

    private static void testConstructorWidthHeightNameColor() {
        BasicFrame frame = new BasicFrame(300, 300, "Constructor test 2", Color.red);
    }

    public static void main(String args[]) {
        testConstructorNoParam();
        testConstructorName();
        testConstructorWidthHeight();
        testConstructorWidthHeightNameColor();
    }

}
