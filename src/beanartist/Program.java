package beanartist;

import beanartist.view.BeAnArtistFrame;

/**
 * Main program class
 */
public class Program {

    public static BeAnArtistFrame mainFrame = null;

    public static void main(String[] args) {
        System.out.println("Start Be An Artist");
        mainFrame = new BeAnArtistFrame("Be An Artist");
    }
}
