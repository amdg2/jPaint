package beanartist.controller;

import beanartist.view.IView;
import beanartist.view.ViewPanel;

import java.util.EmptyStackException;
import java.util.Stack;

/**
 * History controller
 */
public class HistoryController {
    private Stack<IView> views;
    private ViewPanel panel;

    public HistoryController(ViewPanel panel) {
        views = new Stack<>();
        this.panel = panel;
    }

    public void pushView(IView view) {
        if(view != null) {
            views.push(view);
            panel.getBasicFrame().getMenu().updateMenuStatus();
        }
    }

    public IView popView() throws EmptyStackException {
        IView tmp = views.pop();
        panel.getBasicFrame().getMenu().updateMenuStatus();
        return tmp;
    }

    public void removeView(IView view) {
        if(view != null) {
            views.remove(view);
            panel.getBasicFrame().getMenu().updateMenuStatus();
        }
    }

    public boolean isEmpty() {
        return views.isEmpty();
    }

    public void clear() {
        views.clear();
        panel.getBasicFrame().getMenu().updateMenuStatus();
    }
}
