package beanartist.controller;

import beanartist.model.Line;
import beanartist.view.ShapeView;

/**
 * Created by Kevin on 09/11/2015.
 */
public class LineTool extends Tool {
    @Override
    public ShapeView createView() {
        Line line = new Line(this.getStart().getX(),this.getStart().getY(),this.getEnd().getX(),this.getEnd().getY());
        line.setLineColor(this.getColor());
        line.setStroke(this.getStroke());
        return new ShapeView(line);
    }
}
