package beanartist.controller;

import beanartist.controller.strings.ITool;
import beanartist.view.BucketFillView;
import beanartist.view.ViewPanel;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Bucket Fill Tool
 */
public class BucketFillTool implements ITool, MouseListener {

    private ViewPanel panel;
    private Color color;
    private Boolean isFill;
    private Stroke stroke = new BasicStroke();

    @Override
    public void free() {
        if(panel != null)
        {
            panel.setCurrentTool(null);
            panel.removeMouseListener(this);
        }
    }

    @Override
    public void associate(ViewPanel panel) {
        if(panel != null) {
            this.panel = panel;
            this.panel.setCurrentTool(this);
            this.panel.addMouseListener(this);
        }
    }

    @Override
    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public void setFillShape(boolean isFill) {
        this.isFill = isFill;
    }

    @Override
    public void setStroke(Stroke stroke) {
        this.stroke = stroke;
    }

    @Override
    public Stroke getStroke() {
        return this.stroke;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        BucketFillView view = new BucketFillView(e.getPoint(), this.color);
        panel.addView(view);
        panel.getHistory().pushView(view);
        panel.repaint();
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
