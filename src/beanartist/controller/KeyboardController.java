package beanartist.controller;

import beanartist.view.BeAnArtistFrame;

import java.awt.*;
import java.awt.event.KeyEvent;

/**
 * Keyboard controller class
 */
public class KeyboardController implements KeyEventDispatcher {

    private BeAnArtistFrame beAnArtistFrame;

    public KeyboardController(BeAnArtistFrame beAnArtistFrame) {
        this.beAnArtistFrame = beAnArtistFrame;
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_ESCAPE:
                beAnArtistFrame.getViewPanel().unselectTool();
                beAnArtistFrame.getMenuPanel().unselectTool();
                break;
        }

        return false;
    }
}
