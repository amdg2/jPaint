package beanartist.controller;

import beanartist.model.Circle;
import beanartist.view.ShapeView;

import static java.lang.Double.min;
import static java.lang.Math.abs;

/**
 * Created by Kevin on 09/11/2015.
 */
public class CircleTool extends Tool {
    @Override
    public ShapeView createView() {
        double height, width;
        double x,y;

        x=min(this.getStart().getX(), this.getEnd().getX());
        y=min(this.getStart().getY(), this.getEnd().getY());
        //width=abs(this.getStart().getX() - this.getEnd().getX());
        height=abs(this.getStart().getY() - this.getEnd().getY());

        Circle circle =  new Circle(x,y, height, height);
        circle.setLineColor(this.getColor());
        circle.setFillShape(this.isFillShape());
        circle.setStroke(this.getStroke());

        return new ShapeView(circle);
    }
}
