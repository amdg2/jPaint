package beanartist.controller;

import beanartist.model.Rectangle;
import beanartist.view.ShapeView;

import static java.lang.Double.min;
import static java.lang.Math.abs;

/**
 * Created by Kevin on 09/11/2015.
 */
public class RectangleTool extends Tool {


    @Override
    public ShapeView createView() {

        double height, width;
        double x,y;

        x=min(this.getStart().getX(), this.getEnd().getX());
        y=min(this.getStart().getY(), this.getEnd().getY());
        width=abs(this.getStart().getX() - this.getEnd().getX());
        height=abs(this.getStart().getY() - this.getEnd().getY());

        Rectangle rectangle = new Rectangle(x,y, width, height);

        rectangle.setLineColor(this.getColor());
        rectangle.setFillShape(this.isFillShape());
        rectangle.setStroke(this.getStroke());

        return new ShapeView(rectangle);
    }
}
