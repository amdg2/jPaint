package beanartist.controller;

import beanartist.model.Ellipse;
import beanartist.view.ShapeView;

import static java.lang.Double.min;
import static java.lang.Math.abs;

/**
 * Created by Kevin on 09/11/2015.
 */
public class EllipseTool extends Tool {
    @Override
    public ShapeView createView() {
        double height, width;
        double x,y;

        x=min(this.getStart().getX(), this.getEnd().getX());
        y=min(this.getStart().getY(), this.getEnd().getY());
        width=abs(this.getStart().getX() - this.getEnd().getX());
        height=abs(this.getStart().getY() - this.getEnd().getY());

        Ellipse ellipse =  new Ellipse(x,y, width, height);
        ellipse.setLineColor(this.getColor());
        ellipse.setFillShape(this.isFillShape());
        ellipse.setStroke(this.getStroke());

        return new ShapeView(ellipse);
    }
}

