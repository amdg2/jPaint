package beanartist.controller;

import beanartist.controller.strings.ITool;
import beanartist.view.ShapeView;
import beanartist.view.ViewPanel;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

/**
 * Tool abstract class
 */
public abstract class Tool implements ITool, MouseListener, MouseMotionListener {

    private Point end = null;
    private Point start = null;
    private ViewPanel viewPanel;
    private ShapeView tempShapeView = null;
    private boolean creating = false;
    private Color color;
    private boolean isFill;
    private Stroke stroke = new BasicStroke();

    public void associate(ViewPanel panel) {
        if(panel != null) {
            viewPanel = panel;
            viewPanel.setCurrentTool(this);
            viewPanel.addMouseListener(this);
            viewPanel.addMouseMotionListener(this);
        }
    }

    public void free() {
        if(viewPanel != null)
        {
            viewPanel.setCurrentTool(null);
            viewPanel.removeMouseListener(this);
            viewPanel.removeMouseMotionListener(this);
        }
    }

    public abstract ShapeView createView();

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {

    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {
        start = mouseEvent.getPoint();
        creating = true;
    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {
        end = mouseEvent.getPoint();

        if(tempShapeView != null)
            viewPanel.removeView(tempShapeView);

        ShapeView shapeView = this.createView();
        viewPanel.addView(shapeView);
        viewPanel.getHistory().pushView(shapeView);
        viewPanel.repaint();
        creating = false;
    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseDragged(MouseEvent mouseEvent) {
        if(creating) {
            if(tempShapeView != null)
                viewPanel.removeView(tempShapeView);

            end = mouseEvent.getPoint();
            tempShapeView = this.createView();
            viewPanel.addView(tempShapeView);
            viewPanel.repaint();
            end = null;
        }
    }

    @Override
    public void mouseMoved(MouseEvent mouseEvent) {

    }

    public Point getEnd() {
        return end;
    }

    public Point getStart() {
        return start;
    }

    public void setEnd(Point end) {
        this.end = end;
    }

    public void setStart(Point start) {
        this.start = start;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public boolean isFillShape(){
        return isFill;
    }

    public void setFillShape(boolean isFill){
        this.isFill = isFill;
    }

    public void setStroke(Stroke stroke) {
        this.stroke = stroke;
    }

    public Stroke getStroke() {
        return stroke;
    }
}
