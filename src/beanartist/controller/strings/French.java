package beanartist.controller.strings;

import java.util.HashMap;
import java.util.Map;

/**
 * French translations
 */
public class French implements IStrings {

    public French() {
        init();
    }

    private void init() {
        strings = new HashMap<>();

        strings.put("File", "Fichier");
        strings.put("Edit", "Édition");
        strings.put("Open", "Ouvrir");
        strings.put("Save", "Enregistrer");
        strings.put("Close", "Fermer");
        strings.put("Exit", "Quitter");
        strings.put("Copy", "Copier");
        strings.put("Cut", "Couper");
        strings.put("Paste", "Coller");
        strings.put("Fill","Remplir");
        strings.put("Erase","Effacer");
        strings.put("Clear all", "Tout supprimer");
        strings.put("Cancel", "Annuler");

        /**
         * Color Name
         */
        strings.put("Red","Rouge");
        strings.put("Blue","Bleu");
        strings.put("Green","Vert");
        strings.put("Yellow","Jaune");
        strings.put("Black","Noir");
        strings.put("White","Blanc");


        /**
         * Shape
         */
        strings.put("Rectangle","Rectangle");
        strings.put("Square","Carré");
        strings.put("Ellipse","Ellipse");
        strings.put("Line","Ligne");
        strings.put("Circle","Cercle");
        strings.put("Free Hand Line", "Ligne à main levée");
        strings.put("Bucket", "Pot de peinture");

        /**
         * Others
         */
        strings.put("No tool", "Aucun outil");
        strings.put("Unable to save file %s. Format not supported. Only BMP, PNG and JPEG are supported",
                    "Impossible d'enregistrer le fichier %s. Format d'image non supporté.\nSeul BMP, PNG et JPEG sont supportés.");

        strings.put("Unable to open file %s. Format not supported. Only BMP, PNG and JPEG are supported",
                "Impossible d'ouvrir le fichier %s. Format d'image non supporté.\nSeul BMP, PNG et JPEG sont supportés.");

        strings.put("Unable to save file %s.", "Impossible d'enregister le fichier %s.");
        strings.put("Unable to open file %s.", "Impossible d'ouvrir le fichier %s.");
        strings.put("Error while saving image.", "Erreur pendant l'enregistrement de l'image.");
        strings.put("Error while opening image.", "Erreur pendant l'ouverture de l'image.");
        strings.put("Save image...", "Enregister l'image...");
        strings.put("Open image...", "Ouvrir l'image...");
        strings.put("Unable to create tool %s.", "Impossible de créer l'outil %s.");
        strings.put("Error while painting.", "Erreur pendant le dessin.");
        strings.put("Error while painting.\n%s", "Erreur pendant le dessin.\n%s");
    }

    @Override
    public Languages getLanguage() { return Languages.FR; }

    @Override
    public String get(String string) {
        String output = strings.get(string);
        return output == null ? string : output;
    }

    private Map<String, String> strings;
}
