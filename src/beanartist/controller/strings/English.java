package beanartist.controller.strings;

import java.util.HashMap;
import java.util.Map;

/**
 * Provides english translations
 */
public class English implements IStrings {

    public English() {
        init();
    }

    private void init() {
        strings = new HashMap<>();

        strings.put("File", "File");
        strings.put("Edit", "Edit");
        strings.put("Open", "Open");
        strings.put("Save", "Save");
        strings.put("Close", "Close");
        strings.put("Exit", "Exit");
        strings.put("Copy", "Copy");
        strings.put("Cut", "Cut");
        strings.put("Paste", "Paste");
        strings.put("Fill","Fill");
        strings.put("Erase","Erase");


        /**
         * Color Name
         */
        strings.put("Red","Red");
        strings.put("Blue","Blue");
        strings.put("Green","Green");
        strings.put("Yellow","Yellow");
        strings.put("Black","Black");
        strings.put("White","White");

        /**
         * Shape
         */
        strings.put("Rectangle","Rectangle");
        strings.put("Square","Square");
        strings.put("Ellipse","Ellipse");
        strings.put("Line","Line");
        strings.put("Circle","Circle");
    }

    @Override
    public String get(String string) {
        String output = strings.get(string);
        return output == null ? string : output;
    }

    @Override
    public Languages getLanguage() {
        return Languages.EN;
    }

    private Map<String, String> strings;
}
