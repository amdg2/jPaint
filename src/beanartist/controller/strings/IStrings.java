package beanartist.controller.strings;

/**
 * Interface for class that provides languages translation
 */
public interface IStrings {
    String get(String string);
    Languages getLanguage();
}
