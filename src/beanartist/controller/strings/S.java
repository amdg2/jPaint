package beanartist.controller.strings;

/**
 * Provide translations to the whole program
 */
public class S {

    private static IStrings currentLanguage;

    public static String get(String stringName) {
        initLanguageIfNeeded();
        return currentLanguage.get(stringName);
    }

    public static Languages getLanguage() { return currentLanguage.getLanguage(); }

    public static void setLanguage(Languages language) {

        if(language == currentLanguage.getLanguage())
            return;

        switch(language) {
            default:
            case FR:
                currentLanguage = new French();
                break;

            case EN:
                currentLanguage = new English();
                break;
        }
    }

    private static void initLanguageIfNeeded() {
        if(currentLanguage == null)
            currentLanguage = new French();
    }
}
