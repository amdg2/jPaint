package beanartist.controller.strings;

/**
 * Available languages for translation
 */
public enum Languages {
    EN,
    FR
}
