package beanartist.controller.strings;

import beanartist.view.ViewPanel;

import java.awt.*;

/**
 * Tool Interface
 */
public interface ITool {
    void free();
    void associate(ViewPanel panel);
    void setColor(Color color);
    void setFillShape(boolean isFill);
    void setStroke(Stroke stroke);
    Stroke getStroke();
}
