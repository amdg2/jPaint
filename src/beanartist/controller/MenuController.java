package beanartist.controller;

import beanartist.controller.strings.S;
import beanartist.view.BasicFrame;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * Menu controller
 */
public class MenuController {
    private BasicFrame frame;

    public MenuController(BasicFrame frame) {
        this.frame = frame;
    }

    public void openFile() {
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter(
                "BMP, PNG, JPG Image", "bmp", "png", "jpg", "jpeg");
        chooser.setFileFilter(filter);
        chooser.setApproveButtonText(S.get("Open"));
        chooser.setDialogTitle(S.get("Open image..."));
        chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        chooser.setMultiSelectionEnabled(false);

        int returnVal = chooser.showOpenDialog(frame);
        if(returnVal == JFileChooser.APPROVE_OPTION) {
            try {
                String filename = chooser.getSelectedFile().getName();
                if(filename.endsWith("bmp") ||
                        filename.endsWith("png") ||
                        filename.endsWith("jpg") ||
                        filename.endsWith("jpeg"))
                    frame.getViewPanel().setBufferedImage(ImageIO.read(chooser.getSelectedFile()));
                else
                    JOptionPane.showMessageDialog(frame,
                            String.format(S.get("Unable to open file %s. Format not supported. Only BMP, PNG and JPEG are supported"), chooser.getSelectedFile().getName()),
                            S.get("Error while opening image."),
                            JOptionPane.ERROR_MESSAGE);
            } catch (IOException e) {
                JOptionPane.showMessageDialog(frame,
                        String.format(S.get("Unable to open file %s."), chooser.getSelectedFile().getName()),
                        S.get("Error while opening image."),
                        JOptionPane.ERROR_MESSAGE);
                e.printStackTrace();
            }
        }
    }

    public void saveFile() {
        BufferedImage bi = frame.getViewPanel().getBufferedImage();

        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter(
                "BMP, PNG, JPG Image", "bmp", "png", "jpg", "jpeg");
        chooser.setFileFilter(filter);
        chooser.setApproveButtonText(S.get("Save"));
        chooser.setDialogTitle(S.get("Save image..."));
        chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        chooser.setMultiSelectionEnabled(false);

        int returnVal = chooser.showOpenDialog(frame);
        if(returnVal == JFileChooser.APPROVE_OPTION) {
            try {
                String filename = chooser.getSelectedFile().getName();
                if(filename.endsWith("bmp"))
                    ImageIO.write(bi, "bmp", chooser.getSelectedFile());
                else if(filename.endsWith("png"))
                    ImageIO.write(bi, "png", chooser.getSelectedFile());
                else if(filename.endsWith("jpg") || filename.endsWith("jpeg"))
                    ImageIO.write(bi, "jpg", chooser.getSelectedFile());
                else
                    JOptionPane.showMessageDialog(frame,
                            String.format(S.get("Unable to save file %s. Format not supported. Only BMP, PNG and JPEG are supported"), chooser.getSelectedFile().getName()),
                            S.get("Error while saving image."),
                            JOptionPane.ERROR_MESSAGE);
            } catch (IOException e) {
                JOptionPane.showMessageDialog(frame,
                        String.format(S.get("Unable to save file %s."), chooser.getSelectedFile().getName()),
                        S.get("Error while saving image."),
                        JOptionPane.ERROR_MESSAGE);
                e.printStackTrace();
            }
        }
    }

    public void exitApplication() {
        frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
    }

    public void clearView() {
        frame.getViewPanel().clear();
    }

    public void cancelAction() {
        frame.getViewPanel().removeLastView();
    }
}
