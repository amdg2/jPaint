package beanartist.controller;

import beanartist.model.Square;
import beanartist.view.ShapeView;

import static java.lang.Double.min;
import static java.lang.Math.abs;

/**
 * Created by Kevin on 09/11/2015.
 */
public class SquareTool extends Tool {
    @Override
    public ShapeView createView() {
        double height, width;
        double difference = 0;
        double x,y;
        Square square;
        x=min(this.getStart().getX(), this.getEnd().getX());
        y=min(this.getStart().getY(), this.getEnd().getY());
        width=abs(this.getStart().getX() - this.getEnd().getX());
        height=abs(this.getStart().getY() - this.getEnd().getY());

        //difference = abs(height-width);


        if(height>width){
            square = new Square(x,y, width, width);
        }else if (width>height){
            square = new Square(x,y, height, height);
        }else{
            square = new Square(x,y, width, height);
        }



        square.setLineColor(this.getColor());
        square.setFillShape(this.isFillShape());
        square.setStroke(this.getStroke());

        return new ShapeView(square);
    }

}
