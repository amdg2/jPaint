package beanartist.controller;

import beanartist.model.FreehandLine;
import beanartist.view.ShapeView;

import java.awt.event.MouseEvent;

/**
 * Free Hand Line tool class
 */
public class FreeHandLineTool extends Tool {

    FreehandLine model;

    @Override
    public ShapeView createView() {
        if(model == null)
            model = new FreehandLine();
        model.setLineColor(this.getColor());
        model.addSegment(this.getStart().getX(),this.getStart().getY(),this.getEnd().getX(),this.getEnd().getY());

        this.setStart(this.getEnd());
        model.setStroke(this.getStroke());
        return new ShapeView(model);
    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {
        super.mouseReleased(mouseEvent);
        model = null;
    }
}
