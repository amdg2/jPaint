package beanartist.model;

import java.awt.*;

/**
 * Shape, abstract representation
 * of all the object that can be drawn
 */
public interface Shape extends java.awt.Shape {


    Color getLineColor();

    void setLineColor(Color color);

    void setFillShape(boolean isFill);

    boolean isFillShape();

    void setStroke(Stroke stroke);

    Stroke getStroke();

    void moveShapeTo(double x, double y);
}
