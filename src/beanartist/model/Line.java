package beanartist.model;

import java.awt.*;
import java.awt.geom.Line2D;

/**
 * Line class
 */
public class Line extends Line2D.Double implements Shape {


    protected Color color;
    protected Stroke stroke;


    public Line(double x1,double y1,double x2, double y2) {
        super(x1,y1,x2,y2);
    }

    public void moveP1To(double x,double y){
        this.setLine(x,y,this.getX2(),this.getY2());

    }

    public void moveP2To(double x,double y){

        this.setLine(this.getX1(),this.getY1(),x,y);
    }


    @Override
    public Color getLineColor() {
        return color;
    }

    @Override
    public void setLineColor(Color color) {
        this.color = color;
    }


    @Override
    public void setFillShape(boolean bool) {

    }

    @Override
    public boolean isFillShape() {
        return false;
    }

    @Override
    public void setStroke(Stroke stroke) {
        this.stroke = stroke;

    }

    @Override
    public Stroke getStroke() {
        return stroke;
    }

    @Override
    public void moveShapeTo(double x, double y) {
        double vectorX,vectorY;
        vectorX = this.getX2() - this.getX1();
        vectorY = this.getY2() - this.getY1();

        this.setLine(x,y,x + vectorX,y + vectorY);

    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " : "
                + "P1 = (" +  this.getX1() + "," + this.getY1() + ")"
                + " P2 = (" + this.getX2() + "," + this.getY2() + ")."
                ;
    }



}
