package beanartist.model;

import java.awt.*;
import java.awt.geom.Path2D;

/**
 * Free Hand Line model class
 */
public class FreehandLine extends Path2D.Double implements Shape {


    protected Color color;
    protected Stroke stroke;

    public FreehandLine(){

    }

    public void addSegment(double x1, double y1, double x2, double y2){
        Line segment = new Line(x1, y1, x2, y2);
        this.append(segment, true);
    }

    @Override
    public Color getLineColor() {
        return color;
    }

    @Override
    public void setLineColor(Color color) {
        this.color = color;
    }

    @Override
    public void setFillShape(boolean bool) {

    }

    @Override
    public boolean isFillShape() {
        return false;
    }

    @Override
    public void setStroke(Stroke stroke) {
        this.stroke = stroke;

    }

    @Override
    public Stroke getStroke() {
        return stroke;
    }

    @Override
    public void moveShapeTo(double x, double y) {

    }
}
