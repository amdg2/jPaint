package beanartist.model;

import java.awt.geom.Point2D;

/**
 * Created by Kevin on 19/10/2015.
 */
public class Circle extends Ellipse {



    public Circle(){
        super();
    }
    public Circle(double x,double y,double width, double height){
        super(x,y,width,height);
    }

    public Circle(Point2D.Double pos,double width, double height){
        super(pos,width,height);
    }



    @Override
    public String toString() {
        return super.toString();
    }
}
