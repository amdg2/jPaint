package beanartist.model;

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;

/**
 * Ellipse class
 */
public class Ellipse extends Ellipse2D.Double implements Shape {

    /**
     * Constant
     */

    protected final static double DEFAULT_WIDTH = 30;
    protected final static double DEFAULT_HEIGHT = 5;
    protected final static double DEFAULT_X = 50;
    protected final static double DEFAULT_Y = 50;
    protected Color color;
    protected boolean isFill;
    protected Stroke stroke;


    /**
     * Default constructor
     */
    public Ellipse() {
        super(DEFAULT_X,DEFAULT_Y,DEFAULT_WIDTH,DEFAULT_HEIGHT);
    }

    public Ellipse(double x,double y, double width,double height){
        super(x,y,width,height);
    }

    public Ellipse(Point2D.Double pos, double width, double height){
        super(pos.getX(),pos.getY(),width,height);
    }



    /**
     * Override methods
     */

    /**
     *
     * @return
     */
    @Override
    public Color getLineColor() {
        return color;
    }

    /**
     *
     * @param color
     */
    @Override
    public void setLineColor(Color color) {
        this.color = color;
    }

    @Override
    public void setFillShape(boolean isFill) {
        this.isFill = isFill;
    }

    @Override
    public boolean isFillShape() {
        return isFill;
    }

    @Override
    public void setStroke(Stroke stroke) {
        this.stroke = stroke;

    }

    @Override
    public Stroke getStroke() {
        return stroke;
    }

    /**
     *
     * @param x
     * @param y
     */
    @Override
    public void moveShapeTo(double x, double y) {

    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " : "
                + "pos = (" +  this.getX() + "," + this.getY() + ")"
                + " dim = <" + this.getWidth() + "," + this.getHeight() + ">."
                ;
    }
}
