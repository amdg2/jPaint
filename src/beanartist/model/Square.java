package beanartist.model;

import java.awt.geom.Point2D;

/**
 * Created by Kevin on 19/10/2015.
 */
public class Square extends Rectangle {


    /**
     * Constructor
     */
    public Square(){
        super();
    }

    public Square(double x, double y, double width, double height){
        super(x,y,width,height);
    }

    public Square(Point2D.Double pos, double width, double height){
        super(pos,width,height);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
